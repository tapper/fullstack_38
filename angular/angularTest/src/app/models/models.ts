export class User { 
    id:number = 0;
    name:string = "";
    email:string = "";
    phone:string = "";
    website:string = "";
}

export class Post { 
    id:number = 0;
    userId:string = "";
    title:string = "";
    body:string = "";
}



export class ColorOb { 
    red:boolean = true;
    blue:boolean = true;
    green:boolean = false;
    black:boolean = false;
    white:boolean = false;
}

// export class ColorOb {
//     red:boolean = true;
//     blue:boolean = true;
//     green:boolean = false;
//     black:boolean = false

//     constructor(red:boolean,blue:boolean,green:boolean,black:boolean){
//         this.red = red;
//         this.blue = blue;
//         this.green = green;
//         this.black = black;
//     }
// }


export class Show { 
    name:string = "";
}