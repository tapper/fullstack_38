import { Component, Input, OnInit } from '@angular/core';
import { Post } from 'src/app/models/models';

@Component({
  selector: 'app-user-posts-card',
  templateUrl: './user-posts-card.component.html',
  styleUrls: ['./user-posts-card.component.css']
})
export class UserPostsCardComponent implements OnInit {
  
  @Input() componentPost:Post = new Post();
  
  constructor() {
    
   }

  ngOnInit(): void {
    console.log("componentPost : " , this.componentPost)
  }

}
