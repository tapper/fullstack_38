import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/models';
import { ApiService } from 'src/app/services/api/api.service';
import { TestService } from 'src/app/services/test.service';

@Component({
  selector: 'app-api-page',
  templateUrl: './api-page.component.html',
  styleUrls: ['./api-page.component.css']
})


export class ApiPageComponent  {
  constructor(public t:TestService) { }
}
