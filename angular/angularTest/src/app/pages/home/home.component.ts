import { Component, OnInit } from '@angular/core';
import { ColorOb, User } from 'src/app/models/models';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent  {
    
  name:string = "Empty";
  num:number = 30;
  boxColor:string = "red";
  colorsArray:Array<string> = ['gray','orange','yellow','blue','brown']
  arr:Array<number> = [1,6,7,9,19];


//   connectedUser:User = new User("shay",43,"s@gmail.com");
//   connectedUser1:User = new User("michal",23,"m@gmail.com");
  users:Array<User> = [];
  




  //users:User[] = [];

//   users:Array<any> = [
//     {
//         name:"shay",
//         mail:"s@gmail.com",
//         phone:"123456"
//     },
//     {
//         name:"shay",
//         mail:"s@gmail.com",
//         phone:"123456"
//     },
//     {
//         name:"shay",
//         mail:"s@gmail.com",
//         phone:"123456"
//     }
//   ]

  public colors:ColorOb = new ColorOb();
  public colorsArr:Array<ColorOb> = [];


//   colors:any = {
//     red:true,
//     blue:true,
//     green:true
//   }

  constructor() {
    this.colorsArr.push(this.colors);
    this.colors.white = true; 
    // this.users.push(this.connectedUser);
    // this.users.push(this.connectedUser1);
    console.log("USERS : " , this.users)


    //this.colors.black = true;

    // this.connectedUser.name = "shay";
    // this.connectedUser.age = 43;
    // this.connectedUser.mail = "s@gmail.com";

    // this.connectedUser1.name = "michal";
    // this.connectedUser1.age = 23;
    // this.connectedUser1.mail = "m@gmail.com";
  }

  test(){

  }

  changeName(){
    this.name="shay";
  }

  changeValue(i:number){
    this.arr[i]++;
  }

  changeColor(colorName:string){
        if(colorName == "red" || colorName == "green")
            this.colors[colorName] = !this.colors[colorName]

       if(colorName == 'red')
            this.colors.red = !this.colors.red;
        if(colorName == 'blue')
            this.colors.blue = !this.colors.blue;
        if(colorName == 'green')
            this.colors.green = !this.colors.green;
  }


}
