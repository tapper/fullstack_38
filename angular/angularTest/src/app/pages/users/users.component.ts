import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  constructor(public api:ApiService) { 
    this.getUsers();
  }

  ngOnInit(): void {
  }

  async getUsers(){
    let users = await this.api.getService();
    console.log("USERS : " , users)
  }

}
