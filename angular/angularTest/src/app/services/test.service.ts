import { Injectable } from '@angular/core';
import { Post, User } from '../models/models';
import { ApiService } from './api/api.service';

@Injectable({
  providedIn: 'root'
})


export class TestService {

  public num:number = 1;
  public users:Array<User> = [];
  public posts:Array<Post> = [];
  public filterPostsArray:Array<Post> = [];
  
  constructor( public api:ApiService) {
    this.getUsers();
    this.getPosts();
  }
    
  async getUsers(){
    this.users = await this.api.getService("users") as Array<User>;
    console.log("USERS : " , this.users)
  }

  async getPosts(){
    this.posts = await this.api.getService("posts") as Array<Post>;
    //this.filterPostsArray = this.posts;
    //this.filterPostsArray = this.posts.filter(post=>Number(post.userId) == 1);
    this.filterPosts(1)
  }

  filterPosts(userID:number){
    this.filterPostsArray = this.posts.filter(post=>Number(post.userId) == userID);
    console.log("Filter : " , userID ,   this.filterPostsArray , this.posts)
  }


  testServiceFn(){}

  changeNumber(type:number){
    if((this.num > 9 && type == 1) ||  (this.num < 1 && type == 0))
        return;

    this.num = type == 1 ? this.num +1 : this.num -1;
  }

}
