import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
    
    title:string = 'Hello Fullstack 38';
    num:number = 10;
    
    user = {
        name:"shay",
        mail:"s@gmail.com"
    }

    constructor(){
        this.num = this.sum();
    }

    sum(){
        return 34;
    }
}
