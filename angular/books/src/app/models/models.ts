
export class Book { 
    id:number = 0;
    name:string = "";
    pagesQnt:number;
    price:number;
    authorID:number = 0;
}


export class Author{
    id:number = 0;
    name:string = "";
}


// export class User{
//     id:number = 0;
//     name:string = ""; 
//     email:string = ""; 
//     geo:Array<Geo> = [];
//     geo1:Geo[] = [];
// }
// //user.geo.push({lat:"123",lng:"123454"})
// export class Geo{
//     lat:string = "";
//     lng:string = "";
// }

// "id": 1,
// "name": "Leanne Graham",
// "email": "Sincere@april.biz",
// "geo": [{
// "lat": "-37.3159",
// "lng": "81.1496"
// },{
// "lat": "-37.3159",
// "lng": "81.1496"
// },{
// "lat": "-37.3159",
// "lng": "81.1496"
// },{
// "lat": "-37.3159",
// "lng": "81.1496"
// }]
