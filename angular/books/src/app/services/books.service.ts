import { Injectable } from '@angular/core';
import { Author, Book } from '../models/models';
import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  
    public authors:Array<Author> = [{id:1,name:"author1"},{id:2,name:"author2"},{id:3,name:"author3"}];
    public selectedBook:Book = new Book();
    public bookIdCounter:number = 1;
    public books:Array<Book> = [];

    constructor(public api:ApiService) { 
        
        //getBookFromDb
        //getAuthorsFormDb
        //this.getBooksFromDb();
    }
    
    async getBooksFromDb(){
        this.books = await this.api.getService("getbooks") as Array<Book>;
    }

    onSubmit(){

        //addBookdToDb
        this.selectedBook.id = this.bookIdCounter;
        this.books.push(this.selectedBook);
        this.selectedBook = new Book();
        this.bookIdCounter++;

        console.log("BookName : " , this.books)
    }

    getNameByid(authorID:number){
        let author = this.authors.find(a=>a.id == authorID)

        if(author)
            return author.name;
        return "";
    }

    deleteBook(i:number){
        //DeleteBook
        this.books.splice(i,1)
    }
}
