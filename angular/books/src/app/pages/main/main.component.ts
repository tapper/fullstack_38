import { Component, OnInit } from '@angular/core';
import { Author, Book } from 'src/app/models/models';
import { BooksService } from 'src/app/services/books.service';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})

export class MainComponent implements OnInit {
  constructor(public bookService:BooksService) { }
  ngOnInit(): void {}
}
