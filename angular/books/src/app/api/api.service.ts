import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})

export class ApiService {
    baseUrl: string = "http://localhost:5000/"

    constructor(private http:HttpClient) {}
      
    getService(url:string = "") {
      return new Promise(async (resolve, reject) => {
        try {
          await this.http.get(this.baseUrl + url).subscribe(data => {
            resolve(data);
          }, error => {
            console.log('oops', error, error.error)
          });
        } catch (err) {
          console.log(err);
        }
      });
    }
}

