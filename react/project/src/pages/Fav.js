import React, { useState, useEffect} from 'react';
import FavCard from '../components/FavCard';
import { useSelector, useDispatch } from "react-redux";



function Fav() {
  

//   const favorites = useSelector((state) => state.favorites);
    const dispatch = useDispatch();
    const favorites = useSelector((state) => {
        console.log("FAV " , state.favorites)
        return state.favorites
    });

  const iconClicked = (ID) => {
        let fav = structuredClone(favorites)
        let index = fav.findIndex(o=>o.ID == ID)
        if(index >= 0)
           fav[index].isSelected = !fav[index].isSelected;
        console.log("ob" , index , fav)
      

         dispatch({
                 type: "SetFavorites",
                 payload: fav
     });
       // setfavorites(fav)
  }

  return (
    <div>
        <div className='row'>
            {
                favorites.map(f=><FavCard key={f.ID} iconClicked={iconClicked} ob={f} />)
            }
        </div>
    </div>
  )
}

export default Fav