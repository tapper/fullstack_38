
const initialState = {
    favorites:[
        {
            ID:1,
            name:"Fav1",
            isSelected:true,
            count:7
        },
        {
            ID:2,
            name:"Fav2",
            isSelected:false,
            count:8
        },
        {
            ID:3,
            name:"Fav3",
            isSelected:true,
            count:2
        },
        {
            ID:4,
            name:"Fav4",
            isSelected:false,
            count:6
        },
        {
            ID:5,
            name:"Fav5",
            isSelected:true,
            count:8
        },
        {
            ID:6,
            name:"Fav6",
            isSelected:true,
            count:1
        },
        {
            ID:7,
            name:"Fav7",
            isSelected:true,
            count:3
        },
        {
            ID:8,
            name:"Fav8",
            isSelected:false,
            count:7
        }
    ]
};



function rootReducer(state = initialState, action) {
    switch (action.type) {
        case 'SetFavorites':
            console.log("action.payload " , action.payload )
            state = { ...state, favorites: action.payload  }
            break;
    }

    return state;
}

export default rootReducer;









































// import moment from "moment";
// import Utils from '../api/Utils';


// let initState = {
//   scanModalOpen: false,
//   scanned: false,
//   timeStatus: 24,
//   language: "heb",
//   camera: "back",
//   currentDate: "",
//   currentHour: moment().format('hh:mm'),
//   stateButtonText: "WORKER ID",
//   fontLoaded: false,
//   position: "on",
//   version: "0.04",
//   contactText: "",
// }


// var weekDayName = moment().format('dddd');
// var MonthName = moment().format('MMMM');
// var MonthNum = moment().format('MM');
// var DayNum = moment().format('DD');
// var HebFormat = moment().format('DD/MM/YYYY')
// var CurrentYear = moment().format('YYYY');
// initState = { ...initState, currentDate: Utils.TranslateDate(initState.language) + ", " + MonthName + " " + DayNum + ", " + CurrentYear }


// export default reducers = (state = initState, action) => {
//   switch (action.type) {
//     case 'changeModalState':
//       state = { ...state, scanModalOpen: action.payload }
//       break;

//     case 'changeScanedState':
//       state = { ...state, scanned: action.payload }
//       break;

//     case 'ChangeStateButtonText':
//       state = { ...state, stateButtonText: action.payload }
//       break;

//     case 'changeFontLoaded':
//       state = { ...state, fontLoaded: action.payload }
//       break;

//     case 'changePosition':
//       state = { ...state, position: action.payload }
//       break;

//     case 'changeTimeState':
//       state = { ...state, timeStatus: action.payload }
//       let cTime = action.payload == 12 ? moment().format('hh:mm') : moment().format('HH:mm')
//       state = { ...state, currentHour: cTime };
//       break;

//     case 'changeCurrentTime':
//       state = { ...state, currentHour: action.payload }
//       break;

//   }

//   return state;
// }
