import React, { useState, useEffect} from 'react';
import './comp.css';

function FavCard(props) {
  return (
    <div className='col-3'>
        <div className="card">
            <div className="card-body">
                <h5 className="card-title">{props.ob.name} / {props.ob.count}</h5>
                <span className="material-symbols-outlined iconSpan" onClick={()=>props.iconClicked(props.ob.ID)}>
                    { props.ob.isSelected ? "heart_plus" : "favorite" } 
                </span>
            </div>
        </div>
    </div>
  )
}

export default FavCard