import React, { useState, useEffect } from "react";
import {useParams} from 'react-router-dom'


function Show() {
    const [ShowId, setShowId] = useState(0);
    const { id } = useParams();
    const { name } = useParams();

    useEffect(() => {
        
    }, []);

  return (
    <div> SHOW : {id} -- {name}</div>
  )
}

export default Show