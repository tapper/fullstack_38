import React, { useState, useEffect } from 'react';
import { GetRequest } from '../services/Api';
import { useSelector, useDispatch } from "react-redux";
import moment from 'moment'


function Home() {

    const dispatch = useDispatch();
  
    const vacations = useSelector((state) => {
      console.log('vacations : ' , state.vacations)
      return state.vacations
    });

    const user = useSelector((state) => {
        console.log('user : ' , state.user)
        return state.user
    });


    useEffect(() => {
        console.log("Format : " , moment('2023-01-02T21:10:31+02:00').format('DD/MM/YYYY  HH:mm')) 
        // 2023-05-02T21:10:31+02:00
        getUsersDetails();
    }, [])

    const getUsersDetails = async() => {
        let user = await GetRequest('getUserDetailsByUserId?id=98')
        
        dispatch({
            type: "UpdateUser",
            payload: user
        });

        dispatch({
            type: "UpdateVacations",
            payload: user.vacations
        });


        console.log("User : ", user);
        //http://localhost:5000/getUserDetailsByUserId?id=98
    }

  return (
        <div>
            {vacations.map(v=>{
             return <div>
                        <div>{v.destination}</div>
                        <div>{v.isFollow.toString()}</div>
                        <hr/>
                    </div>
            })}
        </div>
  )
}

export default Home