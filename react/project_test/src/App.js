import './App.css';
import { configureStore } from "@reduxjs/toolkit";
import { Provider } from 'react-redux';
import reducers from './redux/reducers';
import Home from './pages/Home';


const store = configureStore({ reducer: reducers })


function App() {
  return (
    <Provider store={store}>
    <div className="App">
        <Home />
    </div>
    </Provider>
  );
}

export default App;
