const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');

var corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200
}

app.use(cors(corsOptions))
app.use(bodyParser.json());


/////////////////////// MONGO DB ////////////////////////
//var url = "mongodb+srv://fullstack:1Qazxsw2@fullstack2023.cema7zf.mongodb.net/"
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////


const Mongo = require('./routes/mongo');
app.use("", Mongo);

app.listen(4000);