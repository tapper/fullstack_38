import React, { useState, useEffect} from 'react';
import UsersTable from '../components/UsersTable';
import { GetRequest, PostRequest } from '../services/Api';
import Swal from 'sweetalert2';

function Home() {
    const [users, setusers] = useState([])

    useEffect(() => {
        getUsers();
    }, []);

    const getUsers = async() =>{
        let r = await GetRequest('users',0);
        setusers(r);
        console.log("R : " , r)
    }
  return (
    <div>
      {users.length > 0 &&  <UsersTable users={users} />} 
    </div>
  )
}

export default Home