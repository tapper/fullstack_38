import React, { useState, useEffect} from 'react';
import { GetRequest } from '../services/Api';
import Swal from 'sweetalert2';

function UsersTable(props) {
    useEffect(() => {
        console.log("P : " , props.users)
    }, []);

    const onSubmit = async(user) => {
       let res =  await GetRequest(`insertUser?name=${user.name}&email=${user.name}&phone=${user.phone}&username=${user.username}`,1)
       console.log("Insert : " , res)

        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'המשתמש נכנס בהצלחה',
            showConfirmButton: false,
            timer: 1500
          })
    }

  return (
    <div className='p-5'>
        <table className="table table-striped-columns">
            <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Phone</th>
                <th scope="col">Username</th>
                <th scope="col">
                   AddToDb
                </th>
            </tr>
            </thead>
            <tbody>
               { 
                    props.users.map(user=>
                            <tr>
                                <td>{user.name}</td>
                                <td>{user.email}</td>
                                <td>{user.phone}</td>
                                <td>{user.username}</td>
                                <td> <button type='button' className='btn btn-primary' onClick={()=>onSubmit(user)}>Add To DB</button></td>
                            </tr>
                    )
                }
            </tbody>
        </table>
  </div>
  )
}

export default UsersTable