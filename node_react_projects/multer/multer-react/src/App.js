import logo from './logo.svg';
import './App.css';
import Vacations from './pages/vacations';

function App() {
  return (
    <div className="App">
        <Vacations />
    </div>
  );
}

export default App;
