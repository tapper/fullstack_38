import React, { useState, useEffect} from 'react';
import axios from 'axios'

function Vacations() {
    const [GlobalUrl, setGlobalUrl] = useState('http://localhost:4000/');
    const [GlobalImageUrl, setGlobalImageUrl] = useState('http://localhost:4000/public/vacation_images/');
    const [VacationName, setVacationName] = useState("");
    const [VacationImage, setVacationImage] = useState("");
    const [Vacations, setVacations] = useState([]);

    useEffect(() => {
        getVacations();
    }, []);

    const getVacations = () =>{
        axios.get(GlobalUrl + "getAllVacations", {
        }).then(res => {
            console.log(res.data)
            setVacations(res.data)
        })
    }

    const onFileChange = (e) =>{
        console.log("E : ", e.target.files)
        setVacationImage(e.target.files[0])
    }

    const onSubmit = ()=> {
        console.log("SUB : " , VacationName ,VacationImage)
        let str = "abc";
        let type = typeof(str);

        let items = [];
        // sort by name

       

        const formData = new FormData()
        formData.append('VacationImage', VacationImage)
        formData.append('VacationName', VacationName)
        axios.post(GlobalUrl + "createVacation", formData, {
        }).then(res => {
            console.log(res)
        })
    }

    sortVacations(arr,'price','string');

    const sortVacations = (arr , parameter, type) => {
        let newItems = arr.sort((a, b) => {
            const nameA = '';
            const nameB = '';

            if(type == 'string')
            {
                nameA = a[parameter].toUpperCase(); // ignore upper and lowercase
                nameB = b[parameter].toUpperCase(); // ignore upper and lowercase
            }else if(type == 'date'){
                nameA = a[parameter].moment(); // ignore upper and lowercase
                nameB = b[parameter].moment();
            }else{
                nameA = a[parameter]; // ignore upper and lowercase
                nameB = b[parameter]; //
            }
          
            
            if (nameA < nameB) {
            return -1;
            }
            if (nameA > nameB) {
            return 1;
            }
        
            // names must be equal
            return 0;
        });

        return newItems;
    }


    return (
        <>
          <div className='row'>
            <div className='col-3'>
                    <div>Add User Form</div>
                    <div className="form-group">
                        <input type="text" placeholder="Enter vacation name" onChange={(e)=>setVacationName(e.target.value )}/>
                    </div>
                    <div className="form-group">
                        <input type="file" onChange={(e) => onFileChange(e)} />
                    </div>
                    <div>
                        <div className="form-group" onClick={() => onSubmit()}>
                            <button className="btn btn-primary" type="submit">Upload Vacation</button>
                        </div>
                    </div>    
            </div>
            <div className='col-9'>
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">VacationId</th>
                        <th scope="col">VacationName</th>
                        <th scope="col">Price</th>
                        <th scope="col">Image</th>
                    </tr>
                </thead>
                <tbody>
                   { Vacations.map((v,i)=> 
                    <tr key={i}>
                        <th>{v.vacationId}</th>
                        <th>{v.vacationDestination}</th>
                        <td>{v.vacationDescription}</td>
                        <td><img src={GlobalImageUrl + v.vacationImage} style={{width:'100px'}}></img></td>

                    </tr> 
                ) }
                </tbody>
                </table>
            </div>
            </div>
        </>
    )
}

export default Vacations