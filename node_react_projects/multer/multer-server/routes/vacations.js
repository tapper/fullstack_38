let express = require('express'),
    multer = require('multer'),
    router = express.Router();

const DIR = './public/vacation_images/';


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + "_" + file.originalname.toLowerCase())
    }
});

var upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
            cb(null, true);
        } else {
            cb(null, false);
            return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
        }
    }
});

const vacationsController = require('../controllers/vacationsController');


///MY FUNCTIONS 

router.post('/createVacation', upload.single('VacationImage'), vacationsController.createVacation);
router.get('/getAllVacations', vacationsController.getAllVacations);

module.exports = router;