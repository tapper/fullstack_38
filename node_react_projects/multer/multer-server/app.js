let express = require('express');
let cors = require('cors');
let bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cors());
app.use('/public', express.static('public'));

const vacations = require('./routes/vacations');
app.use("/", vacations);


app.use((req, res) => {
    res.send('<h1>Page not found</h1>')
})

app.listen(4000);