import logo from './logo.svg';
import './App.css';
import MainPage from './pages/MainPage';
import Clothes from './pages/Clothes';

function App() {
  return (
    <div className="App">
       {/* <MainPage />  */}
       <Clothes />
    </div>
  );
}

export default App;
