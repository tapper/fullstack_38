import React, { useState, useEffect } from 'react';

//rfce
//imrse

function Clothes() {

    let [selectedProduct, setselectedProduct] = useState();
    let [selectedQnt, setselectedQnt] = useState(0);

    let [companies, setcompanies] = useState(
        [{
            id:1,
            name:"nike"
        },
        {
            id:2,
            name:"adidas"
        },
        {
            id:3,
            name:"asics"
        }]
    )


    let [clothes, setclothes] = useState(
        [
            {
                id:1,
                name:"מכנסיים",
                companyId:1,
                color:"שחור",
                price:1000,
                qnt:10,
                size:"XL"
            },
            {
                id:2,
                companyId:3,
                name:"חולצה",
                color:"אדומה",
                price:120,
                qnt:4,
                size:"M"
            },
            {
                id:3,
                companyId:2,
                name:"כובע",
                color:"כחול",
                price:35,
                qnt:4,
                size:"L"
            }
        ]
        )

        const getcompanyNameBycompanyID = (companyID) => {
            let res = companies.find(c=>c.id == companyID);
            return res.name;
        }

        const updateSelectedQnt = (type) => { //1||2||3
            // let s = selectedProduct;
            // s.qnt = selectedQnt;
           

            let cl = clothes;
            let index = clothes.findIndex(c=> c.id == selectedProduct.id)
            cl[index].qnt = selectedQnt;
            
            setclothes(cl);
            setselectedQnt(0);
            setselectedProduct();
        }


  return (
    <div>
        <button className='btn btn-primary' type='button' onClick={()=>setselectedProduct()}>Clear</button>
        <table className="table">
            <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Name</th>
                    <th scope="col">Company</th>
                    <th scope="col">Color</th>
                    <th scope="col">Price</th>
                    <th scope="col">Qnt</th>
                    <th scope="col">Size</th>
                    <th scope="col">Info</th>
                </tr>
            </thead>
            <tbody>
               { 
                    clothes.map(cl=> <tr key={cl.id}>
                            <th>{cl.id}</th>
                            <th>{cl.name}</th>
                            <th>{getcompanyNameBycompanyID(cl.companyId)}</th>
                            <th>{cl.color}</th>
                            <th>{cl.price}</th>
                            <th>{cl.qnt}</th>
                            <th>{cl.size}</th>
                            <th><button className='btn btn-primary' type='button' onClick={()=>setselectedProduct(cl)}>More Info</button></th>
                    </tr> )
                }
            </tbody>
        </table>

        <hr />

        <div>
            {selectedProduct && 
                <div>
                    <div>{selectedProduct.name}</div>
                    <div>{selectedProduct.color}</div>
                    <div>{selectedProduct.price}</div>
                    <div>{selectedProduct.size}</div>
                    <div>{selectedProduct.qnt}</div>
                    <div>
                        <input type="number" onChange={(e)=>setselectedQnt(e.target.value)}></input>
                        <button className='btn btn-primary' type='button' onClick={()=>updateSelectedQnt()}>Update Qnt</button>
                    </div>
                </div>
            }
        </div>
    </div>
  )
}

export default Clothes