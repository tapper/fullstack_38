import React, { useState, useEffect } from 'react';

function MainPage() {
    
  let [products, setproducts] = useState(
    [
        {
            id:1,
            name:"Laptop",
            companyId:1,
            price:2000,
            qnt:10,
        },
        {
            id:2,
            name:"PC",
            companyId:3,
            price:4000,
            qnt:6
        },
        {
            id:3,
            name:"Tablet",
            companyId:2,
            price:1000,
            qnt:3
        }
    ]
  )

  const [companies, setcompanies] = useState(
    [
        {
            id:1,
            name:"apple"
        },
        {
            id:2,
            name:"microsoft"
        },
        {
            id:3,
            name:"dell"
        },
    ]
  )
  
  useEffect(() => {
        let p = products.map(p => {
            p.companyName =  getCompanyName(p.id)
            return p;
        })
        console.log("P : " , p)
        setproducts(p)
  }, []);

  const getCompanyName =(companyID)=>{
    let res = companies.find(c=>c.id == companyID)
    console.log(res , companies , companyID)
    return res.name;
  }

  const setSelectedProduct = (product) => {
    setSelectedProduct(product)
  }

  return (
    <div>
        <table className="table">
        <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Name</th>
                <th scope="col">Company</th>
                <th scope="col">Price</th>
                <th scope="col">Qnt</th>
            </tr>
        </thead>
        <tbody>
            { 
                products.map(prod =>
                    <tr key={prod.id}>
                        <th>{prod.id}</th>
                        <td>{prod.name}</td>
                        {/* <td>{prod.companyId == 1 ? 'APPLE' : prod.companyId == 2 ? "Microsoft" : "Dell"}</td> */}
                        {/* <td>{getCompanyName(prod.companyId)}</td> */}
                        <td>{prod.companyName}</td>
                        <td>{prod.price}</td>
                        <td>{prod.qnt}</td>
                    </tr>
                )
            }
        </tbody>
        </table>
    </div>
  )
}

export default MainPage






















// Example with nodeJS
    // const [products1, setproducts1] = useState([])
    // const [companies1, setcompanies1] = useState([])
    // //http://www.localhost:5000/getproducts
    // //http://www.localhost:5000/getcompanies

    // useEffect(() => {
    //     getProducts();
    //     getCompanies();
    // }, []);

    // const getProducts = async () =>{
    //     let res = await axios.get('http://www.localhost:5000/getproducts')
    //     products1 = res.data;
    // }

    // const getCompanies = async () =>{
    //     let res = await axios.get('http://www.localhost:5000/getcompanies')
    //     companies1 = res.data;
    // }