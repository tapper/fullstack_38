let usersUrl = "https://jsonplaceholder.typicode.com/users"
let PostsUrl = "https://jsonplaceholder.typicode.com/posts";
let users = [];
let posts = [];

function callToAjax(callbackFn,url){
    $.ajax({
        type: 'GET',
        datatype: 'json',
        url: url,
        success: function (data) {
            callbackFn(data)
        },
        error: function (error) {
          console.log('error : ', error)
        },
      })
}


function printUserstohtml(usersData , fillUsers = true){
    usersDV.innerHTML  = "";

    if(fillUsers)
    users = usersData;

    // for(let i=0;i<users.length;i++){
    //     printSingleUser(users[i]) 
    // }

    usersData.map((user,index)=>{
        printSingleUser(user)
    })

    console.log("printUserstohtml : " , users)
}

function setPosts(postsData){
    posts = postsData;
    console.log("Posts : " ,posts )
}

function printSingleUser(user){
    var SingleuserStr = ''
    SingleuserStr += '<div class="col-3">'
    SingleuserStr += '<div class="card" style="border: 1px solid gray; margin-top:10px; cursor:pointer">'
    SingleuserStr += '<div class="card-body">'
    SingleuserStr += '<h5 class="card-title">' + user.name + '</h5>'
    SingleuserStr += `<button type="button" class="btn btn-primary" onClick="printPostsTable(${user.id})"> Posts of ${user.name }</button>`
    SingleuserStr += '</div>'
    SingleuserStr += '</div>'
    SingleuserStr += '</div>'
    usersDV.innerHTML += SingleuserStr
}

function printPostsTable(userId){
    tableRow.innerHTML = "";
    
    let usersPosts = posts.filter(post=> post.userId == userId);

    usersPosts.map(post=>{
        let p = `
            <tr>
                <th scope="row">${post.id}</th>
                <td>${post.title}</td>
                <td>${post.body}</td>
                <td><button type="button" class="btn btn-primary" onClick="printComments(${post.id})"> Comments </button></td>
            </tr>
        `

        tableRow.innerHTML += p;
    })

    console.log(userId , usersPosts)
}

function searchChange(){
    let usersFilter = []; 
    console.log("usersSearchDV : " , usersSearchDV.value.length)
    if(usersSearchDV.value == "")
    {
        console.log("A" , users.length)
        usersFilter = users;
    }
    else {
        let usersClone = [...users];
        usersFilter = usersClone.filter(user=> user.name.toLowerCase().includes(usersSearchDV.value.toLowerCase()));
        console.log("B" , users.length)
    }

    console.log("usersFilter : " , usersFilter)

    printUserstohtml(usersFilter , false)
}

callToAjax(printUserstohtml,usersUrl)
callToAjax(setPosts,PostsUrl)
