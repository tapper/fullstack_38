<?php
    include 'connect.php';

    if(isset($_GET['id']))
    {
        $id = $_GET['id'];
        $sql ="DELETE from students  WHERE id = '$id'";
        $result = mysqli_query($conn,$sql);
    }

    $sql = "SELECT * FROM cars";
    $result = mysqli_query($conn,$sql);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Students Table</h2>
    <table class="table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Phone</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
        <?php while($row = mysqli_fetch_assoc($result)) { ?>
            <tr>
                <td><?php echo $row['id'] ?></td>
                <td><?php echo $row['name'] ?></td>
                <td><?php echo $row['year'] ?></td>
                <td><a href="index.php?id=<?php echo $row['id']?>"> <button type="button" class="btn btn-primary">Edit</button></a></td>
                <td><a href="table.php?id=<?php echo $row['id']?>"> <button type="button" class="btn btn-success">Delete</button></a></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

</body>
</html>