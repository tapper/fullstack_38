<?php
    include 'connect.php';

    if(isset($_GET['id']))
    {
        $id = $_GET['id'];
        $sql ="DELETE from vacations  WHERE vacationId = '$id'";
        echo $sql;
         $result = mysqli_query($conn,$sql);
    }

    $sql = "SELECT * FROM vacations";
    $result = mysqli_query($conn,$sql);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
<a href="insert_vacation.php"> <button type="button" class="btn btn-primary">Add Vacation</button></a></td>
    <div class="row">
        <div class="col-3">
            <form action="insert_vacation.php" >
                <div class="form-group">
                    <label for="exampleInputEmail1">Vacation Destination</label>
                    <input type="text" class="form-control" name="vacationDestination" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Destination">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Vacation Description</label>
                    <input type="text" class="form-control" name="vacationDescription" id="exampleInputPassword1" placeholder="Enter Description">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div> 
        <div class="col-9">
        <h2>Vacation Users Table</h2>
    <table class="table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Destination</th>
            <th>Description</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>

       
        <?php  while($row = mysqli_fetch_assoc($result)) {   ?>
            <tr>
                <td><?php echo $row['vacationId'] ?></td>
                <td><?php echo $row['vacationDestination'] ?></td>
                <td><?php echo $row['vacationDescription'] ?></td>
                <td><a href="index.php?id=<?php echo $row['vacationId']?>"> <button type="button" class="btn btn-primary">Edit</button></a></td>
                <td><a href="table.php?id=<?php echo $row['vacationId']?>"> <button type="button" class="btn btn-success">Delete</button></a></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
        </div>
    </div>
  
</div>

</body>
</html>