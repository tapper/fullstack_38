const cors = require('cors')
const express = require('express');
const con = require('./utils/database')

var app = express();

app.use(cors())
app.use(express.json())

app.route('/').get(async (req, res) => {
    let users = await con.execute(`SELECT * FROM users`)
    res.send(users);
});


app.route('/addUser').get(async (req, res) => {
    let name = req.query.name;
    let phone = req.query.phone;
    let mail =  req.query.mail;

    let insert = await con.execute(`INSERT INTO users(name,phone,mail) VALUES (?,?,?)`, [name,phone,mail])
    res.send(insert[0]);
});


app.route('/deleteUser').get(async (req, res) => {
    id = req.query.id;
    let del = await con.execute(`DELETE FROM users WHERE ID = ${id}`)
    res.send(del[0]);
})


app.route('/updateUser').get(async (req, res) => {
    id = req.query.id;
    newName = req.query.name;
    //,[newName,newPhone]
    let updateUser = await con.execute(`UPDATE users SET name=? WHERE ID = ?`,[newName,id]);
    res.send(updateUser[0]);
})


app.listen(5000);