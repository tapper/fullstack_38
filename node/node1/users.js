
const express = require('express');
var app = express();
var users = [];
var idCounter = 1;


app.route('').get((req, res) => {
    res.send(users);
});

app.route('/addUser').get((req, res) => {
    let user = {
        id:idCounter,
        name:req.query.name
    }

    users.push(user);

    idCounter++;
    res.send("success");
});


app.route('/deleteUser').get((req, res) => {
   let id = req.query.id;
   let index = users.findIndex(user=>user.id == id);

   if(index >= 0){
        users.splice(index,1);
        res.send("delete success");
   }
   else 
     res.send("User not found")
});


app.route('/updateUser').get((req, res) => {
    let id = req.query.id;
    let name = req.query.name;
    let user = users.find(u=>u.id == id);

    if(user)
    {
        user.name = name;
        res.send("User update success")
    }
    else {
        res.send("User not found")
    }
});

app.listen(5000);