const express = require('express');
const router = express.Router();


const BasketController = require('../controllers/BasketController')
router.get('/getBasketProducts', BasketController.getBasketProducts);

module.exports = router;

