const express = require('express');
const app = express();
const cors = require('cors')

app.use(cors())
app.use(express.json())

const UsersRoutes = require('./routes/UsersRoute');
app.use(UsersRoutes);

//const ProductsRoutes = require('./routes/ProductsRoute');
//app.use(ProductsRoutes);



app.use((req, res) => {
    res.send('<h1>Page not found</h1>')
})

app.listen(5000);
