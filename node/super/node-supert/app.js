const express = require('express');
const app = express();
const cors = require('cors')

app.use(cors())
app.use(express.json())

const ProductsRoutes = require('./routes/ProductsRoute');
app.use(ProductsRoutes);

const CategoriesRoutes = require('./routes/CategoriesRoutes');
app.use(CategoriesRoutes);

app.use((req, res) => {
    res.send('<h1>Super project - Page not found</h1>')
})

app.listen(5000);