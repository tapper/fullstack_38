const con = require('../utils/database')

exports.showProducts = async (req, res) => {
    let data;
    if(!req.query.categoryID)
        data = await con.execute(`SELECT * FROM products`)
    else 
        data = await con.execute(`SELECT * FROM products Where categoryID = ${req.query.categoryID}`)
    res.send(data[0]);
}

exports.addProduct = async (req, res) => {
    let categoryID = req.query.categoryID;
    let price = req.query.price;
    let name = req.query.name;
    let description = req.query.description;
    let image = req.query.image;
    let data = await con.execute(`INSERT INTO products(name,categoryID,price,description,image) VALUES (?,?,?,?,?)`, [name,categoryID,price,description,image])
    res.send(data[0]);
}


exports.deleteProduct = async (req, res) => {
    let id = req.query.id; 
    let data = await con.execute(`DELETE FROM products WHERE ID = ${id}`)
    res.send(data[0]);
}
