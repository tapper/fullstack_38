const con = require('../utils/database')

exports.showCategories = async (req, res) => {
    let data = await con.execute(`SELECT * FROM categories`)
    res.send(data[0]);
}

exports.addCategory = async (req, res) => {
    let name = req.query.name;
    let image = req.query.image;
    let color = req.query.color;
    
    let data = await con.execute(`INSERT INTO categories(name,image,color) VALUES (?,?,?)`, [name,image,color])
    res.send(data[0]);
}

exports.deleteCategory = async (req, res) => {
    let id = req.query.id; 
    let data = await con.execute(`DELETE FROM categories WHERE ID = ${id}`)
    res.send(data[0]);
}

exports.updateCategory = async (req, res) => {
    let id = req.query.id;
    let name = req.query.name; 
    let image = req.query.image; 

    let data = await con.execute(`UPDATE categories SET name=?,image=? WHERE ID = ?`,[name,image,id]);
    res.send(data[0]);
}