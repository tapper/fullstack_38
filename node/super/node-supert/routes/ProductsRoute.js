const express = require('express');
const router = express.Router();

const ProductsController = require('../controllers/ProductsController')

router.get('/showProducts', ProductsController.showProducts);
router.get('/addProduct', ProductsController.addProduct);
router.get('/deleteProduct', ProductsController.deleteProduct);

module.exports = router;