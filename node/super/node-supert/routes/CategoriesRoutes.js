const express = require('express');
const router = express.Router();

const CategoriesController = require('../controllers/CategoriesController')

router.get('/showCategories', CategoriesController.showCategories);
router.get('/addCategory', CategoriesController.addCategory);
router.get('/deleteCategory', CategoriesController.deleteCategory);
router.get('/updateCategory', CategoriesController.updateCategory);

module.exports = router;

//localhost:5000/getuser?name=shay