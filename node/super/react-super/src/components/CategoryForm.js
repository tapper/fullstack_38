import React, { useState, useEffect } from "react";
import { GetRequest } from "../services/Api";




function CategoryForm(props) {
    const [name, setName] = useState("")
    const [image, setImage] = useState("")
    const [color, setColor] = useState("")

    const onSubmit = async()=> {
        console.log(name , image , color)
        let addCategories = await GetRequest(`addCategory?name=${name}&image=${image}&color=${color}`);
        props.getAllCategoriesFn();
       // getAllCategories();
    }


    return (
        <form className="text-left p-3">
                <div className="form-group">
                    <label>Add Name</label>
                    <input type="text"  onChange={(e)=>setName(e.target.value)}  className="form-control" id="model"  placeholder="Enter model" />
                </div>

                <div className="form-group">
                    <label>Add Image</label>
                    <input type="text"  onChange={(e)=>setImage(e.target.value)}  className="form-control" id="model"  placeholder="Enter color" />
                </div>

                <div className="form-group">
                    <label>Add Color</label>
                    <input type="text"  onChange={(e)=>setColor(e.target.value)}  className="form-control" id="model"  placeholder="Enter year" />
                </div>

                <button type="button" className="btn btn-primary" onClick={()=>onSubmit()}>Submit</button>
        </form>
    )
}

export default CategoryForm