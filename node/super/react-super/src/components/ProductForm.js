import React, { useState, useEffect } from "react";
import { GetRequest } from "../services/Api";




function ProductForm(props) {
    const [name, setName] = useState("")
    const [image, setImage] = useState("")
    const [description, setDescription] = useState("")
    const [price, setPrice] = useState("")
   

    const onSubmit = async()=> {
        console.log(name , image , description , price)
        let addProduct = await GetRequest(`addProduct?name=${name}&image=${image}&price=${price}&description=${description}&categoryID=${props.id}`);
        props.getProducts();
    }


    return (
        <div>
            { props.id && <form className="text-left p-3">
                    <div className="form-group">
                        <label>Add Name</label>
                        <input type="text"  onChange={(e)=>setName(e.target.value)}  className="form-control" id="model"  placeholder="Enter model" />
                    </div>

                    <div className="form-group">
                        <label>Add Image</label>
                        <input type="text"  onChange={(e)=>setImage(e.target.value)}  className="form-control" id="model"  placeholder="Enter color" />
                    </div>

                    <div className="form-group">
                        <label>Add Description</label>
                        <input type="text"  onChange={(e)=>setDescription(e.target.value)}  className="form-control" id="model"  placeholder="Enter year" />
                    </div>

                    <div className="form-group">
                        <label>Add Price</label>
                        <input type="text"  onChange={(e)=>setPrice(e.target.value)}  className="form-control" id="model"  placeholder="Enter year" />
                    </div>

                    <button type="button" className="btn btn-primary" onClick={()=>onSubmit()}>Submit</button>
            </form> }
        </div>
    )
}

export default ProductForm