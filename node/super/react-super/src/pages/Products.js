import React, { useState, useEffect } from "react";
import {useParams} from 'react-router-dom'
import { useSelector, useDispatch } from "react-redux";
import { GetRequest } from "../services/Api";
import {Link} from 'react-router-dom'
import ProductForm from "../components/ProductForm";
import Swal from 'sweetalert2';



function Products() {
  const { id } = useParams();
  const [categoryName, setCategoryName] = useState("");
  const categories = useSelector((state) => state.categories);
  const products = useSelector((state) => state.products);
  const dispatch = useDispatch();

  useEffect(() => {
        console.log(id , categories)
        let cname = "";

        if(id)
        {
            cname = categories.find(cat=>cat.ID == id);
            // if(!cname)
            //     setCategoryName(cname.Name);
            // console.log("Name:  " ,cname)
        }

        getProducts();
  }, []) 
  
  const getProducts = async() => {
    let products = "";

    if(id)
        products = await GetRequest("showProducts?categoryID="+id);
    else 
        products = await GetRequest("showProducts");

        dispatch({
            type: "SetProducts",
            payload: products
        });
    console.log(products)
  }
  

  const DeleteProduct = async(prodId) =>{
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then(async(result) => {
        if(result.isConfirmed){
            let delcategory = await GetRequest(`deleteProduct?id=${prodId}`);
            console.log("DEL : " , delcategory,result)
            if (delcategory.affectedRows == 1 ) {
                getProducts(); 
              Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
              )
            }else {
                Swal.fire(
                    'Not Deleted!',
                    'Your file has been not deleted. please try again !!',
                    'error'
                  )
            }
        }
      })

    }
  return (
    <div>
        <div>הוספת מוצר קטגוריית  {categoryName}</div>
        <div className="row">
            <div className="col-3">
                <ProductForm  getProducts ={getProducts} id={id}/>
            </div>
            <div className="col-9">
                <table className="table table-dark">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Image</th>
                            <th scope="col">Description</th>
                            <th scope="col">Price</th>
                            <th scope="col">CategoryID</th>
                            <th scope="col">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        { 
                           products.map(prod=> <tr key={prod.ID}>
                                <th>{prod.ID}</th>
                                <td>{prod.name}</td>
                                <td>{prod.image}</td>
                                <td>{prod.description}</td>
                                <td>{prod.price}</td>
                                <td>{prod.categoryID}</td>
                                <td><button type="button" className="btn btn-primary"  onClick={()=>DeleteProduct(prod.ID)} > מחק מוצר </button></td>
                            </tr> ) 
                        }
                    </tbody>
                 </table>
            </div>
        </div>
    </div>
  )
}

export default Products




