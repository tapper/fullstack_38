import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import CategoryForm from "../components/CategoryForm";
import { GetRequest } from "../services/Api";
import {Link} from 'react-router-dom'
import Swal from 'sweetalert2';

function Categories() {
  const dispatch = useDispatch();
  const categories = useSelector((state) => state.categories);

  useEffect(() => {
        getAllCategories()
  }, [])
  

  const getAllCategories = async() => {
    console.log("getAllCategories")
    let categories = await GetRequest("showCategories");

    dispatch({
        type: "SetCategories",
        payload: categories
    });
  }

  const Deletecategory = async(catId) =>{
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then(async(result) => {
        if(result.isConfirmed){
            let delcategory = await GetRequest(`deleteCategory?id=${catId}`);
            console.log("DEL : " , delcategory,result)
            if (delcategory.affectedRows == 1 ) {
              getAllCategories(); 
              Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
              )
            }else {
                Swal.fire(
                    'Not Deleted!',
                    'Your file has been not deleted. please try again !!',
                    'error'
                  )
            }
        }
      })
   



  }

  

  return (
    <div className="row">
        <div className="col-3">
        <Link  to={{pathname: "products/"}} ><button type="button" className="btn btn-primary mt-2"> הצג כל המוצרים </button></Link>
            <CategoryForm getAllCategoriesFn={getAllCategories} />
        </div>
        <div className="col-9">
            <table className="table table-dark">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Image</th>
                            <th scope="col">Color</th>
                            <th scope="col">Show products</th>
                            <th scope="col">Delete category</th>
                        </tr>
                    </thead>
                    <tbody>
                        { 
                           categories.map(cat=> <tr key={cat.ID}>
                                <th>{cat.ID}</th>
                                <td>{cat.Name}</td>
                                <td>{cat.Image}</td>
                                <td>{cat.Color}</td>
                                <td><Link  to={{pathname: "products/"+cat.ID}} ><button type="button" className="btn btn-primary"> הצג מוצרים </button></Link></td>
                                <td><button type="button" className="btn btn-primary" onClick={()=>Deletecategory(cat.ID)}> מחק קטגורייה </button></td>
                            </tr> ) 
                        }
                    </tbody>
            </table>
        </div>
    </div>
  )
}

export default Categories