import logo from './logo.svg';
import './App.css';
import {Route, BrowserRouter, Routes , useParams  } from 'react-router-dom'
import Nav from './components/Nav';
import Categories from './pages/Categories';
import Products from './pages/Products';
import { configureStore } from "@reduxjs/toolkit";
import { Provider } from 'react-redux';
import reducers from './redux/reducers';

//npm install redux react-redux @reduxjs/toolkit

const store = configureStore({ reducer: reducers })

function App() {
  return (
    <div className="App">
         <Provider store={store}>
            <BrowserRouter>
                <Nav />
                <Routes>
                    <Route path='/' element={<Categories />}/>
                    <Route path='/products/:id' element={<Products />}/>
                    <Route path='/products/' element={<Products />}/>
                </Routes>
            </BrowserRouter> 
        </Provider>
    </div>
  );
}

export default App;
