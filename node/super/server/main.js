const cors = require('cors')
const express = require('express');
const con = require('./utils/database')

var app = express();

app.use(cors())
app.use(express.json())

app.route('/showCategories').get(async (req, res) => {
    let data = await con.execute(`SELECT * FROM categories`)
    res.send(data[0]);
});

//npm install -g nodemon
//http://www.localhost:5000/showProducts?categoryID=1
app.route('/showProducts').get(async (req, res) => {
    let data;
    if(!req.query.categoryID)
        data = await con.execute(`SELECT * FROM products`)
    else 
        data = await con.execute(`SELECT * FROM products Where categoryID = ${req.query.categoryID}`)
    res.send(data[0]);
});


app.route('/addCategory').get(async (req, res) => {
    let name = req.query.name;
    let image = req.query.image;
    let color = req.query.color;
    
    let data = await con.execute(`INSERT INTO categories(name,image,color) VALUES (?,?,?)`, [name,image,color])
    res.send(data[0]);
});


app.route('/deleteCategory').get(async (req, res) => {
    let id = req.query.id; 
    let data = await con.execute(`DELETE FROM categories WHERE ID = ${id}`)
    res.send(data[0]);
});


app.route('/updateCategory').get(async (req, res) => {
    let id = req.query.id;
    let name = req.query.name; 
    let image = req.query.image; 

    let data = await con.execute(`UPDATE categories SET name=?,image=? WHERE ID = ?`,[name,image,id]);
    res.send(data[0]);
});


app.route('/addProduct').get(async (req, res) => {
    let categoryID = req.query.categoryID;
    let price = req.query.price;
    let name = req.query.name;
    let description = req.query.description;
    let image = req.query.image;
    let data = await con.execute(`INSERT INTO products(name,categoryID,price,description,image) VALUES (?,?,?,?,?)`, [name,categoryID,price,description,image])
    res.send(data[0]);
});

app.listen(5000);
//http://www.localhost:5000/addProduct?name=קוטג&price=6&categoryID=2&description=תיאור של קוטג&image=kotegImage;
//http://www.localhost:5000/addProduct?name=עוף&price=30&categoryID=1&description=תיאור של עוף&image=chickenImage;