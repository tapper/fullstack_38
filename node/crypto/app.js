
const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const crypto = require('crypto');

const secret = 'abcdefgfgt';
const stringToHash = '123#$*!'
// md5
// base64
const hash = crypto.createHmac('sha256', secret).update(stringToHash).digest('hex');

console.log("Hash : ", hash);


app.use((req, res) => {
    res.status(404).send('<h1>Mazda Project  - Page not found</h1>')
})


app.listen(4000);