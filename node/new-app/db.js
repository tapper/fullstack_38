const cors = require('cors')
const express = require('express');
const con = require('./utils/database')

var app = express();

app.use(cors())
app.use(express.json())

app.route('/').get(async (req, res) => {
    let categories = await con.execute(`SELECT * FROM categories WHERE id > 2`)
    res.send(categories[0]);
});


app.route('/insertCategory').get(async (req, res) => {
    let name = req.query.name;
    let color = req.query.color;
    let image = req.query.image;

    let insert = await con.execute(`INSERT INTO categories(Image,Name,Color) VALUES (?,?,?)`, [image,req.query.name,color])
    res.send(insert[0]);
});



app.listen(5000);

//בנה טבלה של מוצרים לפי התמונה ב WHATSAPP / PHPMYADMIN
//לבנות ממשק בריאקט שמוסיף קטגורייה מוסיף מוצר ומציג טבלה של מוצרים וקטגוריות

