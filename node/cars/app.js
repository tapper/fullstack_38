const cors = require('cors')
const express = require('express');
const Car = require('./models/CarsModel')

var app = express();

app.use(cors())
app.use(express.json())

app.route('/addCar').get((req, res) => {
    let Mazda = new Car(req.query.model,req.query.price,req.query.color,req.query.year);
    Mazda.addToCars()
    //Mazda.deleteCar()
    res.send("success");
});


app.route('/showCars').get((req, res) => {
    res.send(Car.showCars());
});

app.route('/deleteCar').get((req, res) => {
    
});


app.listen(5000);