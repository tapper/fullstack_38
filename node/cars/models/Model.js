
cars = [];

class Car {
    constructor(model , year , price)
    {
        this.model = model;
        this.year = year;
        this.price = price;
    }

    addCarToList = () =>{
        cars.push(this);
    }

    static showallCars = () => {
        return this.cars;
    }
}

module.exports.Car = Car;

