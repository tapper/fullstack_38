const cars = [];
const ID = 1;

module.exports = class Car { 
    id = 0;
    model = "";
    price = 0;
    color = "";
    year = 0;

    constructor(model ,year,color,price){
        this.id = ID;
        this.model = model;
        this.price = price;
        this.color = color;
        this.year = year;

        ID++;
    }

    addToCars(){
        cars.push(this)
        console.log(cars)
    }

    static showCars(){
        return cars;
    }

    static deleteCar(){
        return cars;
    }
}